### `Step 1`

Run command "yarn" to install dependencies packages.

### `Step 2`

Run command "yarn start" to run the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


### `Note`

Because this challenge is a demo, so I omitted setting up the eslint for checking syntax.

If there has any problem with running or setting up project. Please contact to me via email address: phuthanh19951@gmail.com 