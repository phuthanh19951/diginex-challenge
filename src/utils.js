export const mergeTwoArray = (array1, array2, key) => {
    const mappedArray = new Map([...array1.map((item) => [item[key], item])]);

    array2.forEach((item) => {
        const mapItem = mappedArray.get(item[key]);
        if (mapItem) Object.assign(mapItem, item);
        else mappedArray.set(item[key], item);
    });
    return [...mappedArray.values()];
  }