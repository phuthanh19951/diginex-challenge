import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store';
import createRoutes from './routes';
import AppRoute from './components/AppRoute/AppRoute';

function App() {
    return (
      <Provider store={store}>
          <Router>
            <AppRoute routes={createRoutes()} />
          </Router>
      </Provider>
    );
}

export default App;