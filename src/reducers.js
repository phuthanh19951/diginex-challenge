import { combineReducers } from 'redux';
import WebSocKetReducer from './redux/websocket/reducer';

export default combineReducers({
    WebSocKetReducer
});