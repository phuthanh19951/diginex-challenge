import { createStore } from 'redux';
import reducers from './reducers';

const store = createStore(
  reducers,
  process.env.NODE_ENV === "production" ? {} : window.devToolsExtension ? window.devToolsExtension() : (f) => { return f; },
);

export default store;