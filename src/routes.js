import NotFound from './components/NotFound/NotFound';
import CryptoCurrencyContainer from './page/CryptoCurrency/CryptoCurrencyContainer'

const createRoutes = () => {
  return [
    {
      path: '/',
      exact: true,
      component: CryptoCurrencyContainer
    },
    {
      component: NotFound,
    },
  ];
};


export default createRoutes;