import { WS_CONNECT_FAILED, WS_CONNECT, WS_CONNECT_SUCCESS } from "./constants";

const initialState = {
    socket: null,
    host: '',
    message: ''
}

export default (state = initialState, action) => {
    switch(action.type){
        case WS_CONNECT:
            return Object.assign({}, state, {
                socket: new WebSocket(action.payload.host),
                host: action.payload.host,
                message: 'Socket is being connected'
            });
        case WS_CONNECT_SUCCESS:
            return Object.assign({}, state, {
                message: 'Socket is connected'
            });  
        case WS_CONNECT_FAILED:
            return Object.assign({}, state, {
                socket: null,
                host: null,
                message: 'Socket connection is failed. Please try it again!'
            });               
        default:
            return state;    
    }
}