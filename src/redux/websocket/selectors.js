  
import { createSelector } from 'reselect';
import { get } from 'lodash';

export const getSocketInstance = createSelector(
  (state) => { return get(state.WebSocKetReducer, 'socket'); },
  (result) => { return result; },
);