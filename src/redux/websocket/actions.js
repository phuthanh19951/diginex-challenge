import * as actionTypes from "./constants";

export const createSocketConnection = (payload) => {
    return {
        type: actionTypes.WS_CONNECT,
        payload
    }
}

export const createSocketConnectionSuccess = () => {
    return {
        type: actionTypes.WS_CONNECT_SUCCESS
    }
}

export const createSocketConnectionFailed = () => {
    return {
        type: actionTypes.WS_CONNECT_FAILED
    }
}