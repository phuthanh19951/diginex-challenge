import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import CryptoCurrency from './CryptoCurrency';
import { createSocketConnection, createSocketConnectionSuccess } from '../../redux/websocket/actions';
import { getSocketInstance } from '../../redux/websocket/selectors';

function CryptoCurrencyContainer(props) {
  let [socket, setSocket] = useState(null);

  let dispatch = useDispatch();
  let socketInstance = useSelector(getSocketInstance);

  if(!socket && socketInstance){
    dispatch(createSocketConnectionSuccess());
    setSocket(socketInstance);
  }

  useEffect(() => {
    if(!socket){
      dispatch(createSocketConnection({ host: 'wss://stream.binance.com:9443/ws/!ticker@arr'}));
    }
  }, [socket]);

  return (
    socket ? <CryptoCurrency ws={socket}/> : 'Loading'
  );
}

export default CryptoCurrencyContainer;
