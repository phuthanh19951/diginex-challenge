import React, { useState, useMemo, useEffect } from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import ReactTable from "react-table";
import "react-table/react-table.css";
import { mergeTwoArray } from '../../utils';
import './index.css';

function CryptoCurrency(props) {
  const { ws } = props;

  let [cryptoes, setCryptoes] = useState([]);
  let accessorColumns = ['s', 'A', 'B', 'C', 'E', 'F', 'L', 'O', 'P', 'Q', 'a', 'b', 'c', 'e', 'h', 'l', 'n', 'o', 'p', 'q', 'v', 'w', 'x' ];

  let columns = useMemo(
    () => accessorColumns.map((column) => {
        return  {
            Header: column,
            accessor: column,
            filterable: column === 's',
            sortable: column !== 's'
          } 
        }),
    [accessorColumns]
  )

  let data = useMemo(() => cryptoes, [cryptoes]);
    
  ws.onopen = () => {
   console.log('Connected');
  }
  
  useEffect(() => {
    ws.onmessage = evt => {
      let data = mergeTwoArray(cryptoes, JSON.parse(evt.data), "s");
      setCryptoes(data);
    }  
  }, [cryptoes]);

  return (
      <div className="tableWrap">
        <ReactTable
            data={data}
            columns={columns}
            pageSize={15}
            showPagination={true}
            showPageJump={false}
            showPageSizeOptions={false}
            className="-striped -highlight"
          />
      </div>
  )
}

CryptoCurrency.propTypes = {
  ws: PropTypes.object.isRequired,
}

export default CryptoCurrency;
