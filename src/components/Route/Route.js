import React from 'react';
import { Route as ReactRoute } from 'react-router-dom';

function Route (props){
    return (
        <ReactRoute {...props} />
    );

}

export default Route;