import React from 'react';
import PropTypes from 'prop-types';
import { Switch as ReactSwitching } from 'react-router-dom';
import Route from '../Route/Route';

function AppRoute(props){
    const { routes } = props;

    return (
        <ReactSwitching>
          { routes.map((route, index) => {
            return <Route key={index} path={route.path} component={route.component} />;
          }) }
        </ReactSwitching>
      );
}

AppRoute.propTypes = {
  routes: PropTypes.array.isRequired,
};

export default AppRoute;